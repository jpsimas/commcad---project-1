#+TITLE: Project 1 Report
#+SUBTITLE: 01NVTOQ - Computer aided design of communication systems
#+AUTHOR: João Pedro de Omena Simas
#+DATE:
#+OPTIONS: toc:nil

\pagebreak

\tableofcontents

\pagebreak

* Part 1 - Ideal Laser
** Item 1
   By simulating the system with the ideal setup with the optical
   amplifier, NRZ ideal pulses,matched optical filter and the
   photodiode with N = 20000 transmitted symbols the following BER vs
   SNR curve was estimated, both using the optimum threshold and the
   empirical one. Below is the plot of said curves together with the
   expected BER curve.

   #+CAPTION: BER results for the ideal setup.
   [[../src/EX1_ber.png]]

   To determine the empirical threshold, the following eye diagram was
   plotted for -42 dBm received power:

   \pagebreak
   #+CAPTION: Eye diagram of received signal for P_{rx} = -42 dBm and the before-mentioned ideal setup.
   [[../src/EX1_eye.png]]
   \pagebreak

   Therefore the average eye opening was estimated visually to be
   \(10^5\). And therefore using the empirical rule of the threshold
   being 0.25 of this opening, we get a threshold on  \(2.5 \times
   10^4\).

   The optimum threshold was obtained using MATLAB's /fminsearch/
   function and finding the value which minimized the BER, when used
   in the detector.

   Using the specified target reference BER of \(3 \times 10^{-4}\) We
   can calculate the sensitivity, using the optimum threshold, to be
   approximately *-42.3 dBm*. 
** Item 2
   By introducing the specified super Gaussian filter and re-running
   the same simulation descibed before, we obtain the BER curve
   represented below: 

   #+CAPTION: BER results after adding the super Gaussian optical filter.
   [[../src/EX2_ber.png]]
   
   From these results, the obtained sensitivity was estimated to be of
   approximately *-42.2 dBm*.
** Item 3
   After adding the band limitation of the electrical driver of
   \(B_{el,-3dB} = 7 GHz\) to the previous setup and re-running the
   simulations we get the following BER curve: 
   #+CAPTION: BER results after adding the band limitation.
   [[../src/EX3_ber.png]]

   This time the obtained sensitivity was *-40.5 dBm*.
** Item 4
   By adding the electrical amplifier and filter and optimizing the
   sensitivity by varying the bandwidth of the receiver electrical
   filter, we get a optimal value of approximately B_{el} = 8.29
   GHz. 
   This was done using the fminbnd function together to the
   simulation funciton (opt_sim.m) written for the previous exercises.
   
   #+CAPTION: BER results after adding the electrical amplifier and filter.
   [[../src/EX4_ber.png]]

   \pagebreak

   The obtained sensitivity was approximately *-48.3 dBm*.
** Item 5

   By estimating the sensitivity in function of gain of the optical
   amplifier we get the following curves:
   #+CAPTION: Sensitivity of the receiver in function of the gain of the optical amplifier.
   [[../src/EX5_sens.png]]

   \pagebreak

   Note that without the electrical noise at G_{opt} = 0 dB,
   the BER goes to 0, because the noise introduced by the optical
   amplifier is proportional to G_{opt} - 1 and therefore has null
   power at this point, so without it's presence and the presence of
   electrical noise there's no noise and therefore no errors.

** Item 6
   By removing the optical amplifier and optimizing the sensitivity by
   varying the bandwith of the bandwith of the electrical filter an
   bandwidth of B_{el} = 5.43 GHz was obtained. Using this value and
   still keeping the optical amplifier off, we get the following BER
   curve.
   #+CAPTION: BER results after optimizing the bandwidth of the electrical filter and without the optical amplifier.
   [[../src/EX6_ber.png]]

   Also, the obtained sensitivity was of approximately *-24.4 dBm*.
** Item 7
   
   Using each of the previously mentioned values, we can calculate the
   maximum fiber length by calculating the length of fiber whose total
   attenuation causes the power in the output to be equal to the
   sensitivity when the input power is 7dBm. Below is the attenuation
   curve indicating the points obtained on each of the previous
   exercises.
   #+CAPTION: Maximum reachable distance for a transmit power of 7dB with points relative to the sensitivities obtained in the previous exercises highlighted.
   [[../src/EX7_lmax.png]]
   
   \pagebreak

* Part 2 - Real Laser
** Item 1
   By plotting the P-I response of the laser we get the following
   curve:
   #+CAPTION: P-I response of the real laser.
   [[../src/EX2_1_pi.png]]
   
   \pagebreak

   To carry out the output mean power an extinction ratio analyses in
   terms of input signal amplitude the input bias current was kept at
   70 mA and the signal amplitude was varied linearly. The results are
   plotted below. 
   #+CAPTION: Plot of the extinction ratio in function of the signal amplitude with i_{bias} = 70mA.
   [[../src/EX2_1_extratio_iampl.png]]
   #+CAPTION: Plot of the output power in function of the signal amplitude with i_{bias} = 70mA.
   [[../src/EX2_1_p_iampl.png]]
   \pagebreak

   To evaluate the effects of varying the input bias an input signal
   amplitude of 80 mApp was used and the input bias current was varied
   linearly. The results can be seen below:
   #+CAPTION: Plot of the extinction ratio in function of the bias current with i_{ampl} = 80 mApp.
   [[../src/EX2_1_extratio_bias.png]]
   #+CAPTION: Plot of the output power in function of the bias current with i_{ampl} = 80 mApp.
   [[../src/EX2_1_p_ibias.png]]
   \pagebreak
** Item 2
   By optimizing the sensitivity, by varying the two current levels
   that represent bits '0' and '1' the values I_{low} = 30.7mA and
   I_{high} = 90.3 mA approximately were obtained, which translate to
   an amplitude of *59.4 mApp* and a bias current of *60.5mA*.

** Item 3
   To analyze the effects of oscillations on the sampling instant in
   relation to the optimal one, an artificial delay was added to the
   signal after the calculation of the optimal sampling instant and
   the BER at received power of P_{rx} = -50dBm was estimated thought
   simulation. The results are plotted below, where T_s is the symbol
   time.
   #+CAPTION: BER in function of the sampling instant noise.
   [[../src/EX2_3_sens_sampl_noise.png]]
   
   To analyze the effects of oscillations on the threshold an white
   Gaussian random signal was added to the threshold at
   decision. Below, the BER at the same reference power of -50dBm is
   plotted against the relative power of said noise in relation to the
   actual optimal threshold calculated by the receiver.

\pagebreak
   #+CAPTION: BER in function of the relative threshold noise.
   [[../src/EX2_3_sens_thr_noise.png]]
   

\pagebreak

* Appendix 1 - Code Listing 
  The full listing of the octave code used for the simulations can be
  found in the post ceding pages. It can be also found at the git
  repository https://gitlab.com/jpsimas/commcad---project-1.git.
** main.m
   This file runs all the needed simulations using the 
   #+INCLUDE: "../src/main.m" src octave
** opt\textunderscore system.m
   This function runs a simulations of the optical
   transmitter/receiver system without the detection and outputs the
   signal after alignment and down sampling.
   #+INCLUDE: "../src/opt_system.m" src octave
** ber\textunderscore count.m
   This function, for a given threshold performs the detection of the
   signal, compares the obtained bits with the transmitted ones,
   counts the errors and estimates the BER.
   #+INCLUDE: "../src/ber_count.m" src octave
** calc\textunderscore ber.m
   This function calculates the threshold for the detection of a given
   demodulated optical signal that minimizes the BER and returns said
   optimum BER.
   #+INCLUDE: "../src/calc_ber.m" src octave
** calc\textunderscore sens.m
   This function estimates the sensitivity of the system, given
   sampled BER curve by interpolating it.
   #+INCLUDE: "../src/calc_sens.m" src octave

* Generate Zip Script :noexport:
# pack all needed files into a zip archive

#+BEGIN_SRC shell :noexport :results silent
cd ../
zip -FS Project1.zip \
./report/report.pdf \
./src/main.m \
./src/opt_system.m \
./README.txt \
./src/ber_count.m \
./src/calc_ber.m \
./src/calc_sens.m \
./src/filterParameters.m \
./src/ideal_laser.p \
./src/ideal_laser_lookup.csv \
./src/laser.p \
./src/laser_lookup.csv
cd -
#+END_SRC
