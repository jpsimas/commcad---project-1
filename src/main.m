%% -*- octave -*-
%% commcad-project-1
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 

clear all;
close all;
format long e;

%%needed if using octave
currver = ver;
isOctave = strcmp(currver(1).Name, 'Octave');
if(isOctave)
  pkg load signal;
  pkg load communications;
end;

sps = 13;

%%PART 1

%%EXERCISE 1
disp("EX1");
n = 40000;

bit_rate = 1.25*10*1e9; %%bit/s
bits = single(randi([0 1], n, 1));

%%reference ber for sensitivity calculations
ber_ref = 3e-4;

%%line attenuation (for exercise 7)
line_alpha_db = 0.2;%%dB/km
P_tx = 1e-3*10^(7/10);%%W

%%low/high power levels of the laser
levels = [50; 100];%%in mA

input_filter_enabled = false;
real_laser = false;
supergaussian_optical_filter = false;
electrical_ampl_and_filter_enabled = false;
optical_ampl_and_filter_enabled = true;
disable_electrical_noise = false;
plot_signals = false;
add_sampling_instant_noise = false;
sigma_sampling_instant_noise = 0;
add_thr_noise = false;
sigma_thr = 0;

%%fiber
P_rx = 1e-3*10.^(linspace(-52, -42, 10)/10);
%%Electrical filter
BW_el_filter = 7e9;
%%Optical Amplifier
G_opt = 1e5;

[y y_full] = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampling_instant_noise, input_filter_enabled,...
	       real_laser, supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise);

plot_ex1_eyes = false;

if(plot_ex1_eyes)
  if(isOctave)
    eyediagram(y(:,end), 1, 1);
  else
      ed = comm.EyeDiagram('SamplesPerSymbol', sps,'DisplayMode',...
			   '2D color histogram','OversamplingMethod',...
			   'Input interpolation','YLimits',...
			   [min(y_full(:,end)) max(y_full(:,end))], 'ColorScale',...
			   'Logarithmic');
      ed(y_full(:,end));
      pause;
  end;
end;
clear y_full;

%%values obtained visually by looking at the graphs
thr = 1e5/4;
ber_manual = ber_count(thr, y, bits);

%%ber using optimal threshold
ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);

h = 6.62607015e-34;%%plancks constant
lambda_c = 1550e-9;%%m
c = 299792458;
f_c = c/lambda_c;
BW_opt_filter = 50e9;%%Hz
n_sp = 2;
sigma_n = sqrt((n_sp*h*f_c*(G_opt - 1)*bit_rate));%%per polarization (sqrt(W))

ber_ideal = 0.5*exp(-0.98*G_opt*P_rx./(2*sigma_n^2));
figure;
hold on;
plot(10*log10(P_rx/1e-3), 10*log10(ber_ideal));

plot(10*log10(P_rx/1e-3), 10*log10(ber_manual));

plot(10*log10(P_rx/1e-3), 10*log10(ber));

xlabel("P_{RX} (dBm)");
ylabel("10 log_{10}(BER)");

legend("expected", "estimated with empirical threshold",...
       "estimated with optimum thresold");
print -dpng EX1_ber.png

sens = calc_sens(P_rx, ber, ber_ref);
sens_dbm = 10*log10(sens*1e3)
%%max dist (for exercise 7)
L_max = (10/line_alpha_db)*log10(P_tx/sens)
L_max_ex(1) = L_max;
sens_ex(1) = sens;

close all;
%%EXERCISE 2
P_rx = 1e-3*10.^(linspace(-50, -40, 10)/10);
disp("EX2");
supergaussian_optical_filter = true;

y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampling_instant_noise, input_filter_enabled,...
	       real_laser, supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise);

ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);


ber_ideal = 0.5*exp(-0.98*G_opt*P_rx./(2*sigma_n^2));
figure;
hold on;
plot(10*log10(P_rx/1e-3), 10*log10(ber_ideal));

plot(10*log10(P_rx/1e-3), 10*log10(ber));
xlabel("P_{RX} (dBm)");
ylabel("10 log_{10}(BER)");

legend("expected", "estimated");
print -dpng EX2_ber.png

sens = calc_sens(P_rx, ber, ber_ref);
sens_dbm = 10*log10(sens*1e3)
L_max = (10/line_alpha_db)*log10(P_tx/sens)
L_max_ex(2) = L_max;
sens_ex(2) = sens;

close all;
%%EXERCISE 3
disp("EX3");
P_rx = 1e-3*10.^(linspace(-50, -40, 5)/10);

input_filter_enabled = true;

y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampling_instant_noise, input_filter_enabled,...
	       real_laser, supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise); 

ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);

ber_ideal = 0.5*exp(-0.98*G_opt*P_rx./(2*sigma_n^2));
figure;
hold on;
plot(10*log10(P_rx/1e-3), 10*log10(ber_ideal));

plot(10*log10(P_rx/1e-3), 10*log10(ber));
xlabel("P_{RX} (dBm)");
ylabel("10 log_{10}(BER)");

legend("expected", "estimated");
print -dpng EX3_ber.png

sens = calc_sens(P_rx, ber, ber_ref);
sens_dbm = 10*log10(sens*1e3)
L_max = (10/line_alpha_db)*log10(P_tx/sens)
L_max_ex(3) = L_max;
sens_ex(3) = sens;

close all;
%%EXERCISE 4
disp("EX4");
electrical_ampl_and_filter_enabled = true;

P_rx = 1e-3*10.^(linspace(-60, -47, 10)/10);

%%[BW_el_filter, sens] = fminbnd(@(bw)calc_sens(P_rx, calc_ber(P_rx,...
%%opt_system(bits, sps, bit_rate, P_rx, bw, G_opt, levels,...
%%sigma_sampling_instant_noise, input_filter_enabled, real_laser,...
%%supergaussian_optical_filter, electrical_ampl_and_filter_enabled,...
%%optical_ampl_and_filter_enabled, disable_electrical_noise,...
%%plot_signals, add_sampling_instant_noise), bits, sigma_thr,...
%%add_thr_noise), ber_ref), bit_rate/10, bit_rate, optimset('TolX',...
%%1e-4, 'MaxFunEvals', 1000))

%%just using pre-calculated value to speed up stuff
B_el_filter = 8.287985894423572e+09;%%3.905764772741397e+09

BW_el_filter_ex4 = BW_el_filter;%%save value for exercise 2-1

y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampling_instant_noise, input_filter_enabled,...
	       real_laser, supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise); 

ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);

figure;
hold on;
plot(10*log10(P_rx/1e-3), 10*log10(ber));

disable_electrical_noise = true;
y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampling_instant_noise, input_filter_enabled,...
	       real_laser, supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise); 

ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);

plot(10*log10(P_rx/1e-3), 10*log10(ber));
xlabel("P_{RX} (dBm)");
ylabel("10 log_{10}(BER)");

legend("with noise", "without noise");
print -dpng EX4_ber.png

sens = calc_sens(P_rx, ber, ber_ref);
sens_dbm = 10*log10(sens*1e3)
L_max = (10/line_alpha_db)*log10(P_tx/sens)
L_max_ex(4) = L_max;
sens_ex(4) = sens;

close all;
%%EXERCISE 5
disp("EX5");
close all;

disable_electrical_noise = false;

G_opt = 10.^(linspace(0, 50, 10)/10);

figure(1);
hold on;
sens = zeros(size(G_opt));
L_max = zeros(size(G_opt)); 
for i = 1:length(G_opt)
  if i == 1
    P_rx = 1e-3*10.^(linspace(-30, -20, 10)/10);
  else
    if i == 2
      P_rx = 1e-3*10.^(linspace(-40, -30, 10)/10);
    else
      if i == 3
	P_rx = 1e-3*10.^(linspace(-50, -40, 10)/10);
      else
	P_rx = 1e-3*10.^(linspace(-55, -45, 10)/10); 
      end;
    end;
  end;
  
  y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt(i), levels,...
		 sigma_sampling_instant_noise, input_filter_enabled,...
		 real_laser, supergaussian_optical_filter,...
		 electrical_ampl_and_filter_enabled,...
		 optical_ampl_and_filter_enabled,...
		 disable_electrical_noise, plot_signals,...
		 add_sampling_instant_noise); 
  ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);
  sens(i) = calc_sens(P_rx, ber, ber_ref);

  L_max(i) = (10/line_alpha_db)*log10(P_tx/sens(i));
  
  plot(10*log10(P_rx/1e-3), 10*log10(ber));
  xlabel("P_{RX} (dBm)");
  ylabel("10 log_{10}(BER)");
end;

figure(1);
legend("0 dB", "12.5 dB", "25 dB", "37.5 dB", "50 dB");
print -dpng EX5_bers_with_noise.png;
hold off;
close;

sens

figure(2);
hold on;
plot(10*log10(G_opt), 10*log10(sens/1e-3), '-x');
xlabel("G_{opt} (dB)");
ylabel("sens (dBm)");

disable_electrical_noise = true;

figure(1);
hold on;
G_opt = G_opt(2:end);%%0dB will just give BER = 0 here, because theres no noise
sens = zeros(size(G_opt));
L_max = zeros(size(G_opt)); 
for i = 1:length(G_opt)
  P_rx = 1e-3*10.^(linspace(-55, -45, 5)/10);
  
  y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt(i), levels,...
		 sigma_sampling_instant_noise, input_filter_enabled,...
		 real_laser, supergaussian_optical_filter,...
		 electrical_ampl_and_filter_enabled,...
		 optical_ampl_and_filter_enabled,...
		 disable_electrical_noise, plot_signals,...
		 add_sampling_instant_noise); 
  ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);
  sens(i) = calc_sens(P_rx, ber, ber_ref);

  L_max(i) = (10/line_alpha_db)*log10(P_tx/sens(i));
  
  plot(10*log10(P_rx/1e-3), 10*log10(ber));
  xlabel("P_{RX} (dBm)");
  ylabel("10 log_{10}(BER)");
end;

figure(1);
legend("12.5 dB", "25 dB", "37.5 dB", "50 dB");
print -dpng EX5_bers_without_noise.png;
close;

sens

figure(2);
plot(10*log10(G_opt), 10*log10(sens/1e-3),'-x');

legend("with noise", "without noise");
print -dpng EX5_sens.png

close all;
%%EXERCISE 6
disp("EX6");
optical_ampl_and_filter_enabled = false;

disable_electrical_noise = false;


P_rx = 1e-3*10.^(linspace(-33,-23, 10)/10);

%%[BW_el_filter, sens] = fminbnd(@(bw)calc_sens(P_rx, calc_ber(P_rx,...
%%opt_system(bits, sps, bit_rate, P_rx, bw, G_opt, levels,...
%%sigma_sampling_instant_noise, input_filter_enabled, real_laser,...
%%supergaussian_optical_filter, electrical_ampl_and_filter_enabled,...
%%optical_ampl_and_filter_enabled, disable_electrical_noise,...
%%plot_signals, add_sampling_instant_noise), bits, sigma_thr,...
%%add_thr_noise), ber_ref), bit_rate/10, bit_rate, optimset('TolX',...
%%1e-4, 'MaxFunEvals', 1000)) 

%%just using pre-calculated value to speed up stuff
BW_el_filter = 5.428928032866938e+09%%5.725767967515616e+09

y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampling_instant_noise, input_filter_enabled,...
	       real_laser, supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise);

ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);

figure;
hold on;

plot(10*log10(P_rx/1e-3), 10*log10(ber));
xlabel("P_{RX} (dBm)");
ylabel("10 log_{10}(BER)");

legend("estimated");
print -dpng EX6_ber.png

sens = calc_sens(P_rx, ber, ber_ref);
sens_dbm = 10*log10(sens*1e3)
L_max = (10/line_alpha_db)*log10(P_tx/sens)
L_max_ex(5) = L_max;
sens_ex(5) = sens;


%%EXERCISE 7 aready there
disp("EX7");
sens = logspace(log10(min(sens_ex)), log10(max(sens_ex)), 100);
L_max_vect = (10/line_alpha_db)*log10(P_tx./sens);

figure;
hold on;
plot(10*log10(sens), L_max_vect);
for i = 1:5
  plot(10*log10(sens_ex(i)), L_max_ex(i), 'x')
end;
legend("", "ex 1", "ex 2", "ex 3", "ex 4", "ex 6")
title("Maximum Distance vs sensitivity");
xlabel("Sensitivity (dBm)");
ylabel("L_{max} (km)");

print -dpng EX7_lmax.png

%%PART 2
input_filter_enabled = true;
real_laser = true;
supergaussian_optical_filter = true;
electrical_ampl_and_filter_enabled = true;
optical_ampl_and_filter_enabled = true;

%%use parameters from ex4
BW_el_filter = BW_el_filter_ex4;

close all;
%%EXERCISE 1
disp("EX2-1");
P_rx = 1e-3*10.^(linspace(-60, -40, 5)/10);
BW_el_filter = BW_el_filter_ex4;
G_opt = 1e5;

i_laser = linspace(-100, 100, 1000);

%%p_laser = laser(i_laser);
laser_lookup = csvread("laser_lookup.csv");
p_laser = interp1(laser_lookup(:, 1), laser_lookup(:, 2), i_laser);
figure;
plot(i_laser, p_laser);
title("Output Power vs Input current of laser");
xlabel("Input Current (mA)")
ylabel("Ouput Power (mW)");
print -dpng EX2_1_pi.png

%%power analysis (unsing 70mA bias)
i_bias = 70;
ampl_i = linspace(40, 120, 10); 
p_laser = (interp1(laser_lookup(:, 1), laser_lookup(:, 2),...
		   ampl_i/2 + i_bias) +...
	   interp1(laser_lookup(:, 1), laser_lookup(:, 2)...
		   , -ampl_i/2 + i_bias));
figure;
plot(ampl_i, p_laser);
title(["Mean Output Power (mW) vs Input Signal Amplitude (mApp) "...
       "with i_{bias} = 70mA"]); 
ylabel("P_{out} (mW)");
xlabel("i_{in} (mApp)");
print -dpng EX2_1_p_iampl.png


ext_ratio = (interp1(laser_lookup(:, 1), laser_lookup(:, 2),...
		     ampl_i/2 + i_bias) ./ ...
	     interp1(laser_lookup(:, 1), laser_lookup(:, 2)...
		     , -ampl_i/2 + i_bias)); 
plot(ampl_i, 10*log10(ext_ratio));
title(["Extinction Ratio (dB) vs Input Signal Amplitude (mApp) "...
       "with i_{bias} = 70 mA"]);
ylabel("r_e (dB)");
xlabel("I_{in} (mApp)");
print -dpng EX2_1_extratio_iampl.png

ampl_i = 40;
i_bias = linspace(30, 120, 10); 
p_laser = (interp1(laser_lookup(:, 1), laser_lookup(:, 2),...
		   ampl_i/2 + i_bias) +...
	   interp1(laser_lookup(:, 1), laser_lookup(:, 2)...
		   , -ampl_i/2 + i_bias)); 
figure;
plot(i_bias, p_laser);

title(["Mean Output Power (mW) vs Input Signal Bias (mA) with"...
       " i_{in} = 40mApp"]); 
ylabel("P_{out} (mW)");
xlabel("i_{bias} (mA)");

print -dpng EX2_1_p_ibias.png

ext_ratio = (interp1(laser_lookup(:, 1), laser_lookup(:, 2),...
		     ampl_i/2 + i_bias) ./ ...
	     interp1(laser_lookup(:, 1), laser_lookup(:, 2)...
		     , -ampl_i/2 + i_bias));
plot(i_bias, 10*log10(ext_ratio));
title(["Extinction Ratio (dB) vs Input Signal Bias (mApp) with"...
       " i_{in} = 40mApp"]); 
ylabel("r_e (dB)");
xlabel("i_{bias} (mA)");

print -dpng EX2_1_extratio_bias.png

close all;
%%EXERCISE 2
disp("EX2-2")

%%[levels, sens] = fminsearch(@(lvs)calc_sens(P_rx, calc_ber(P_rx,...
%%opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, [lvs(1);
%%lvs(2)], sigma_sampling_instant_noise, input_filter_enabled,...
%%real_laser, supergaussian_optical_filter,...
%%electrical_ampl_and_filter_enabled, optical_ampl_and_filter_enabled,...
%%disable_electrical_noise, plot_signals, add_sampling_instant_noise),...
%%bits, sigma_thr, add_thr_noise), ber_ref), [70; 80],...
%%optimset('TolX', 1e-4, 'MaxFunEvals', 10)) 

[lv1, sens] = ...
fminbnd(@(lv)calc_sens(P_rx, calc_ber(P_rx, opt_system(bits, sps, bit_rate,...
						       P_rx,...
						       BW_el_filter,...
						       G_opt, [lv; 100
							      ],...
						       sigma_sampling_instant_noise,...
						       input_filter_enabled,...
						       real_laser,...
						       supergaussian_optical_filter,...
						       electrical_ampl_and_filter_enabled,...
						       optical_ampl_and_filter_enabled,...
						       disable_electrical_noise,...
						       plot_signals,...
						       add_sampling_instant_noise),...
				      bits, sigma_thr, add_thr_noise),...
		       ber_ref), 10, 140, optimset('TolX', 1e-4,...
						   'MaxFunEvals',...
						   100)) 

[lv2, sens] = ...
fminbnd(@(lv)calc_sens(P_rx, calc_ber(P_rx, opt_system(bits, sps, bit_rate,...
						       P_rx,...
						       BW_el_filter,...
						       G_opt, [lv1; lv
							      ],...
						       sigma_sampling_instant_noise,...
						       input_filter_enabled,...
						       real_laser,...
						       supergaussian_optical_filter,...
						       electrical_ampl_and_filter_enabled,...
						       optical_ampl_and_filter_enabled,...
						       disable_electrical_noise,...
						       plot_signals,...
						       add_sampling_instant_noise),...
				      bits, sigma_thr, add_thr_noise),...
		       ber_ref), 10, 140, optimset('TolX', 1e-4,...
						   'MaxFunEvals',...
						   100))  

[lv1, sens] = ...
fminbnd(@(lv)calc_sens(P_rx, calc_ber(P_rx, opt_system(bits, sps, bit_rate,...
						       P_rx,...
						       BW_el_filter,...
						       G_opt, [lv; lv2
							      ],...
						       sigma_sampling_instant_noise,...
						       input_filter_enabled,...
						       real_laser,...
						       supergaussian_optical_filter,...
						       electrical_ampl_and_filter_enabled,...
						       optical_ampl_and_filter_enabled,...
						       disable_electrical_noise,...
						       plot_signals,...
						       add_sampling_instant_noise),...
				      bits, sigma_thr, add_thr_noise),...
		       ber_ref), 10, 140, optimset('TolX', 1e-4,...
						   'MaxFunEvals',...
						   100))  

[lv2, sens] =...
fminbnd(@(lv)calc_sens(P_rx, calc_ber(P_rx, opt_system(bits, sps, bit_rate,...
						       P_rx,...
						       BW_el_filter,...
						       G_opt, [lv1; lv
							      ],...
						       sigma_sampling_instant_noise,...
						       input_filter_enabled,...
						       real_laser,...
						       supergaussian_optical_filter,...
						       electrical_ampl_and_filter_enabled,...
						       optical_ampl_and_filter_enabled,...
						       disable_electrical_noise,...
						       plot_signals,...
						       add_sampling_instant_noise),...
				      bits, sigma_thr, add_thr_noise),...
		       ber_ref), 10, 140, optimset('TolX', 1e-4,...
						   'MaxFunEvals',...
						   100))

levels = [lv1; lv2]

y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampling_instant_noise, input_filter_enabled,...
	       real_laser, supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise);

ber = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise)
sens = calc_sens(P_rx, ber, ber_ref)
sens_dbm = 10*log10(sens*1e3)
L_max = (10/line_alpha_db)*log10(P_tx/sens)

sigma_n = sqrt((n_sp*h*f_c*(G_opt - 1)*bit_rate));%%per polarization (sqrt(W))
ber_ideal = 0.5*exp(-0.98*G_opt*P_rx./(2*sigma_n^2));

figure;
hold on;

%%plot(10*log10(P_rx/1e-3), 10*log10(ber_ideal));
plot(10*log10(P_rx/1e-3), 10*log10(ber));
xlabel("P_{RX} (dBm)");
ylabel("10 log_{10}(BER)");

%%legend("expected", "estimated");

print -dpng EX2_2ber.png

close all;
%%EXERCISE 3
disp("EX2-3")
P_rx = 1e-3*1e-5;%%-50dBm

sens_orig = sens;

sigma_sampl = sqrt(10.^(linspace(-50, -30, 20)/10));
add_sampling_instant_noise = true;

%%n_realiz = 10;
%%sens = zeros(size(sigma_sampl));
ber = zeros(size(sigma_sampl));
for i = 1:length(sigma_sampl)
  %%  for k = 1:n_realiz
  y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
		 sigma_sampl(i), input_filter_enabled, real_laser,...
		 supergaussian_optical_filter,...
		 electrical_ampl_and_filter_enabled,...
		 optical_ampl_and_filter_enabled,...
		 disable_electrical_noise, plot_signals,...
		 add_sampling_instant_noise);
  ber(i) = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);
  %%ber(i) += calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise);
  %%sens(i) = sens(i) + calc_sens(P_rx, ber, ber_ref);
  %%  end;
  %%sens(i) = sens(i)/n_realiz;
  %%ber(i) = ber(i)/n_realiz;
end;

figure;
%%plot(20*log10(sigma_sampl), 10*log10(sens/1e-3));
plot(20*log10(sigma_sampl), 10*log10(ber));
title("BER vs Sampling instant error");
ylabel("10 log10 (BER)");
xlabel("20 log10 (t_{err}/T_s)");

print -dpng EX2_3_sens_sampl_noise.png

add_sampling_instant_noise = false;
sigma_sampl = 0;

sigma_thr = sqrt(10.^(linspace(-50, 0, 10)/10));
add_thr_noise = true;

y = opt_system(bits, sps, bit_rate, P_rx, BW_el_filter, G_opt, levels,...
	       sigma_sampl, input_filter_enabled, real_laser,...
	       supergaussian_optical_filter,...
	       electrical_ampl_and_filter_enabled,...
	       optical_ampl_and_filter_enabled,...
	       disable_electrical_noise, plot_signals,...
	       add_sampling_instant_noise);

%%sens = zeros(size(sigma_thr));
ber = zeros(size(sigma_thr));
for i = 1:length(sigma_thr)
  ber(i) = calc_ber(P_rx, y, bits, sigma_thr(i), add_thr_noise);
  %%sens(i) = calc_sens(P_rx, ber, ber_ref);
end;

figure;
hold on;
%%plot(20*log10(sigma_thr), 10*log10(sens/1e-3));
plot(20*log10(sigma_thr), 10*log10(ber));

title("BER vs Relative threshold error standard deviation");
ylabel("10 log10 (BER)");
xlabel("20 log10 (sigma_{err}/threshold)");

print -dpng EX2_3_sens_thr_noise.png
