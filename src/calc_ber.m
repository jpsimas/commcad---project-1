%% -*- octave -*-
%% commcad-project-1
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 

function [ber] = calc_ber(P_rx, y, bits, sigma_thr, add_thr_noise)
  ber = zeros(length(P_rx), 1);
  
  for i = 1:length(P_rx)
    m = double(mean(y(:,i)));
    [thr, ber(i)] = fminsearch(@(thr)ber_count(thr, y(:,i), bits), m,...
			       optimset('TolX', 1e-4));
    %%thr
    %%ber(i) = ber_count(thr, y(:,i), bits);
    if(add_thr_noise)
      new_thr = (1 + sigma_thr*randn(size(y(:,i))))*thr;
      ber(i) = ber_count(new_thr, y(:,i), bits);
    end;
  end;
end

