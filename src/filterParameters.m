function [At, Bt, Ar, Br, NTranSym] = filterParameters(Sps, filterType, varargin)

if nargin > 2
    Rsym = varargin{1};
    M = varargin{2};       % The length of the raised cosine impulse response is 2*Ns*(M+1)+1 samples
    rho = varargin{3};
    alphat = varargin{4};  % Exponent of the raised cosine characteristics (transmit filter)
    alphar = varargin{5};  % Exponent of the raised cosine characteristics (receive filter)
end

switch filterType
    case 'Rectangular NRZ'
        %% Rectangular filter parameters
        %--- Initialization of the rectangular FIR filters (see Matlab 'filter' function)
        %--- TX side
        Bt = ones(1, Sps);  % Numerator coefficient vector
        At = 1;           % Denominator coefficient vector
        %--- RX side
        Br = ones(1, Sps);  % Numerator coefficient vector
        Ar = 1;           % Denominator coefficient vector
        %--- Number of symbols affected by transient (TX + RX)
        NTranSym = 2;

    case 'Rectangular RZ'
        %% Rectangular filter parameters
        %--- Initialization of the rectangular FIR filters (see Matlab 'filter' function)
        %--- TX side
        DC = floor(Sps/2);
        Bt = [ones(1, DC) zeros(1, Sps-DC)];  % Numerator coefficient vector
        At = 1;           % Denominator coefficient vector
        %--- RX side
        Br = [zeros(1, Sps-DC) ones(1, DC)];  % Numerator coefficient vector
        Ar = 1;           % Denominator coefficient vector
        %--- Number of symbols affected by transient (TX + RX)
        NTranSym = 2;

    case 'Nyquist ISI-free'
        %% Rasied cosine (FIR) filter
        plotMe = 'Yes';
        %--- TX side
        [Bt, At, t] = RaisedCosineFIR(Rsym, Sps, M, rho, alphat, plotMe);
        %--- RX side
        [Br, Ar, t] = RaisedCosineFIR(Rsym, Sps, M, rho, alphar, 'No');
        %--- Number of symbols affected by transient (TX + RX)
        NTranSym = 2*2*M+1;   
        
    case 'arc-of-sine'
        %% Arc of sine fitler
        %--- TX side
        n = 0:Sps-1;
        Bt = sin(pi*n/Sps);  % Numerator coefficient vector
        At = 1;             % Denominator coefficient vector
        %--- RX side
        n = Sps-1:-1:0;
        Br = sin(pi*n(end:-1:1)/Sps); % Numerator coefficient vector
        Ar = 1;                      % Numerator coefficient vector
        %--- Number of symbols affected by transient (TX + RX)
        NTranSym = 2; 
 
    otherwise
        disp('Error: fitler type not supported')
        return;
end