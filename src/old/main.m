%% -*- octave -*-
%% commcad-project-1
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 

%load packages (only needed when using octave)
%pkg load signal;
%pkg load communications;

clear all;
close all;
format long e;

input_filter_enabled = false;
real_laser = false;
supergaussian_optical_filter = false;
electrical_ampl_and_filter_enabled = false;
plot_signals = true;

find_optimal_bw_el_filter = false;

n_realiz = 1;
n = 3000;

bit_rate = 1.25*10*1e9; %bit/s

%fiber
% L = 250;%km
% x_lineout = x_lp*10^(-0.2*L/10);
% P_rx = 1e-3*10.^((-45:5:25)/20);
P_rx = 1e-3*10.^(linspace(-15,3, 20)/20);

%Electrical filter
if(find_optimal_bw_el_filter)
  BW_el_filter = bit_rate*10.^(linspace(-4, 0, 20)/10);
else
  BW_el_filter = bit_rate*10^(-0.2);
end;

%Optical Amplifier
G_opt = 1e5;

ber_mean = zeros(length(P_rx), length(BW_el_filter));
thr_mean = zeros(length(P_rx), length(BW_el_filter));

for realiz = 1:n_realiz

  b = randi([0 1], n, 1);
  
  %modulation
  levels = [50; 100];
  x = levels(1)*(1 - b) + levels(2)*b;

%upsampling
  sps = 16;%upsampling factor
  x_upsampled = zeros(sps*length(x) + (sps - 1), 1);
  x_upsampled(sps:sps:end) = x;

  clear x;

%pulse shaping
  filterType = 'Rectangular NRZ';
  [a_t, b_t, a_r, b_r, NTranSym] = filterParameters(sps, filterType);
  b_r = b_r/dot(b_r, b_r);

  x_ps = filter(b_t, a_t, x_upsampled);
  x_ps = x_ps(length(b_t):end);

  if(plot_signals)
    scatterplot(x_ps);
    title("pulse shaped signal");
  end;

  clear x_upsampled;

  %bandwidth limitation
  BW_lp = 7e9;
  [b_lp_s, a_lp_s] = besself(4, 1.52*2*pi*BW_lp/(bit_rate*sps));
  [b_lp,a_lp] = impinvar(b_lp_s, a_lp_s);
  
  if(input_filter_enabled)
    x_lp = filter(b_lp, a_lp, x_ps);
  else
    x_lp = x_ps;
  end;

  if(plot_signals)
    scatterplot(x_lp);
    title("signal after low-pass");
  end;

  if(real_laser)
    x_laser = laser(x_lp);
  else
    x_laser = ideal_laser(x_lp);
  end;

  if(plot_signals)
    scatterplot(x_laser);
    title("signal at laser o/p");
  end;

  clear x_lp;

  %fiber
  x_lineout = x_laser*sqrt(P_rx/var(x_laser));

  if(plot_signals)
    scatterplot(x_lineout(:,1));
    title("line o/p");
  end;
  
  clear x_laser;

%optical amplifier
  h = 6.62607015e-34;%plancks constant
  lambda_c = 1550e-9;%m
  c = 299792458;
  f_c = c/lambda_c;
  BW_opt_filter = 50e9;%Hz
  n_sp = 2;

  sigma_n = sqrt((2*n_sp*h*f_c*(G_opt - 1)*bit_rate));%per polarization

				%optical filter
				%matched filter
  if(supergaussian_optical_filter)
    BW_opt_filter = 50e9;%passband
    gauss_order = 4;
    
		    % sigma = 1/((2*pi*BW_opt_filter)/(bit_rate*sps));
    sigma = 2*pi*(BW_opt_filter/2/(bit_rate*sps))/(2*log(2))^(1/gauss_order);

    n_w = 8*sps;
    w_gauss = zeros(n_w,1);
    ind = mod((0:(n_w-1))+ n_w/2, n_w)-n_w/2;

    h_gauss = exp(-1j*2*pi/n_w * (0:(n_w-1))) .* exp(-0.5 * ((2*pi*ind/n_w/sigma).^2).^gauss_order);
    
    w_gauss = real(ifft(h_gauss));
    
				% normalize to get unit dc gain
    w_gauss = w_gauss/sum(w_gauss);
    x_opt_filter = filter(w_gauss, [1], x_lineout);
		 %x_opt_filter = x_opt_filter(length(w_gauss):end, :);
  else
    x_opt_filter = filter(b_r, a_r, x_lineout);
    x_opt_filter = x_opt_filter(length(b_r):end, :);
  end;

  if(plot_signals)
    scatterplot(x_opt_filter(1+7:sps:length(x_opt_filter),end));
    title("optical filter o/p");
  end;

  clear x_lineout;
  clear w_gauss;
  clear h_gauss;

				%Photodiode
  R = 0.75;%A/W
  x_x = x_opt_filter + sigma_n*randn(length(x_opt_filter), 1);
  x_y = sigma_n*randn(length(x_opt_filter), 1);
  x_photodiode = R*G_opt*(x_x.*conj(x_x) + x_y.*conj(x_y));

  if(plot_signals)
    scatterplot(x_photodiode(1:sps:length(x_photodiode),end));
    title("photodiode o/p");
  end;

  clear x_opt_filter;
  clear x_x;
  clear x_y;

  if(electrical_ampl_and_filter_enabled)
    % TIA (electrical amp)
    r_t = 3e3;%ohm
    s_n = 20e-12;%A/sqrt(Hz)
    for i = 1:length(BW_el_filter)
      x_tia_out(:,:,i) = r_t*x_photodiode + s_n*sqrt(2*BW_el_filter(i))*randn(length(x_photodiode), 1);
    end;

    if(plot_signals)
      scatterplot(x_tia_out(1:sps:length(x_tia_out),1));
      title("tia o/p");
    end;

    clear x_photodiode;
    
    % Electrical Filter

    for i = 1:length(BW_el_filter)
      [b_el_s(:, :, i), a_el_s(:, :, i)] = besself(4, 1.52*2*pi*BW_el_filter(i)/(bit_rate*sps));
    end;
    for i = 1:length(BW_el_filter)
      [b_el(:,:,i), a_el(:,:,i)] = impinvar(b_el_s(:,:,i), a_el_s(:,:,i));
      x_el_filter_out(:,:,i) = filter(b_el(:,:,i), a_el(:,:,i), x_tia_out(:,:,i));
    end;

    if(plot_signals)
      scatterplot(x_el_filter_out(1:sps:length(x_el_filter_out),1));
      title("electrical filter o/p");
    end;

    clear x_tia_out;
  else
    BW_el_filter = [];
				%x_el_filter_out = x_tia_out;
    x_el_filter_out = x_photodiode;
    clear x_photodiode;
  end;

				%sampling
				%align
  x_ref = filter(b_r, a_r, x_ps);
  x_ref = x_ref(length(b_r):end);
  [corr, corrLag] = xcorr(x_ref, x_el_filter_out(:,end));
  [corrMax, posCorrMax] = max(abs(corr));
  deltaSamp = corrLag(posCorrMax);
  phi_est = angle(corr(posCorrMax));

  hi = exp(-j*(2*pi*(0:(length(x_el_filter_out)-1))*deltaSamp/length(x_el_filter_out)-phi_est)).';
  x_el_filter_out_fft = fft(x_el_filter_out);

  x_el_filter_out_corrected = zeros(size(x_el_filter_out));

  bodge_size = size(x_el_filter_out_corrected);

  if(length(bodge_size) == 3)
    for k = 1:bodge_size(3)
      for i = 1:bodge_size(2)
	x_el_filter_out_corrected(:,i,k) = real(ifft(hi.*x_el_filter_out_fft(:,i,k)));
      end;
    end;
  else
    for i = 1:bodge_size(2)
      x_el_filter_out_corrected(:,i) = real(ifft(hi.*x_el_filter_out_fft(:,i)));
    end;    
  end;

  if(plot_signals)
    scatterplot(x_el_filter_out_corrected(1:sps:length(x_el_filter_out_corrected),end));
    title("corrected signal");
  end;

  x_downsampled = x_el_filter_out_corrected(1:sps:length(x_el_filter_out_corrected), :, :);

  clear x_ps;
  %clear x_el_filter_out;
  clear x_ref;
  clear corr;
  clear corrLag;
  clear hi;
  clear x_el_filter_out_fft;

  %normalization
  for k = 1:length(BW_el_filter)
    for i = 1:length(P_rx)
      x_downsampled(:,i,k) = x_downsampled(:,i,k)./std(x_downsampled(:,i,k));
    end;
  end;

  if(plot_signals)
    scatterplot(x_downsampled(:,end));
    title("downsampled signal (power normalized)");
  end;

  ber = zeros(length(P_rx), 1);

  for i = 1:length(P_rx)
    for k = 1:length(BW_el_filter)
      %threshold
      [thr, ber(i, k)] = fminsearch(@(thr)ber_count(thr, x_downsampled(:,i,k), b), 0.67, optimset('TolX', 1e-4));
      %ber
    end;
  end;

  clear x_downsampled;

  ber_mean = ber_mean + ber;
  thr_mean = thr_mean + thr;

end;

ber_mean = ber_mean/n_realiz
thr_mean = thr_mean/n_realiz

%sensitivity calculation (@ BER = -10dB)
ber_ref = 1e-1;
sens = zeros(length(BW_el_filter), 1);
for k = 1:length(BW_el_filter)
  m = min(ber_mean(:,k));
  if(m <= 1e-1)
    % DANGER: do not look direcly at the bodge
    sens(k) = interp1(ber_mean(:,k)+1e-12*randn(size(ber_mean(:,k))), P_rx.', ber_ref);
  else
    sens(k) = inf;
  end;
end;
[sens_opt, k_opt] = min(sens);
sens_opt
BW_el_filter_opt = BW_el_filter(k_opt);
BW_el_filter_opt_dB = 10*log10(BW_el_filter_opt/bit_rate)

figure;
plot(20*log10(P_rx/1e-3), 10*log10(ber_mean(:,k_opt)));
xlabel("P_{RX} (dBm)");
ylabel("10 log_{10}(BER)");

ber_ideal = 0.5*exp(-0.98*P_rx./(sigma_n^2));
hold on;
plot(20*log10(P_rx/1e-3), 10*log10(ber_ideal));
legend("estimated", "expected");

% clear all;

