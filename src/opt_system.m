%% -*- octave -*-
%% commcad-project-1
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 

function [x_downsampled x_el_filter_out_corrected] = opt_system(bits, sps, bit_rate, P_rx,...
				      BW_el_filter, G_opt, levels,...
				      sigma_sampling_instant_noise,...
				      input_filter_enabled, real_laser,...
				      supergaussian_optical_filter,...
				      electrical_ampl_and_filter_enabled,...
				      optical_ampl_and_filter_enabled,...
				      disable_electrical_noise,...
				      plot_signals,...
				      add_sampling_instant_noise)
  
  %%sps = 13;%%upsampling factor
  %%normalize frequencies
  fs = bit_rate*sps;
  bit_rate = bit_rate/fs;
  BW_el_filter = BW_el_filter/fs;
  
  n_sampl = length(bits);
  %%modulation
  x = levels(1)*(1 - bits) + levels(2)*bits;%%in mA

  %%upsampling
  x_upsampled = single(zeros(sps*length(x) + (sps - 1), 1));
  x_upsampled(sps:sps:end) = x;
  
  clear x;

  %%pulse shaping
  filterType = 'Rectangular NRZ';
  [a_t, b_t, a_r, b_r, NTranSym] = filterParameters(sps, filterType);
  b_r = b_r/dot(b_r, b_r);

  x_ps = filter(b_t, a_t, x_upsampled);
  x_ps = x_ps(length(b_t):end);

  if(plot_signals)
    figure;
    plot(x_ps);
    title("pulse shaped signal");
  end;

  clear x_upsampled;

  %%bandwidth limitation
  BW_lp = 7e9/fs;
  [b_lp_s, a_lp_s] = besself(4, 1.52*2*pi*BW_lp);
  [b_lp,a_lp] = impinvar(b_lp_s, a_lp_s);
  
  if(input_filter_enabled)
    x_lp = filter(b_lp, a_lp, x_ps);
  else
    x_lp = x_ps;
  end;

  if(plot_signals)
    figure;
%%%    scatterplot(x_lp);
    plot(x_lp);
    title("signal after low-pass (mA)");
  end;
  
  if(real_laser)
    %%x_laser = laser(x_lp);
    laser_lookup = csvread("laser_lookup.csv");
    x_laser = interp1(laser_lookup(:, 1), laser_lookup(:, 2), x_lp,...
		      'pchip', 'extrap');
    clear laser_lookup;
  else
    %%x_laser = ideal_laser(x_lp);
    laser_lookup = csvread("ideal_laser_lookup.csv");
    x_laser = interp1(laser_lookup(:, 1), laser_lookup(:, 2), x_lp,...
		      'pchip', 'extrap');
    clear laser_lookup;
  end;

  %%convert from mW to W
  x_laser = x_laser*1e-3;
  
  if(plot_signals)
    figure;
    %%scatterplot(x_laser);
    plot(x_laser);
    title("signal at laser o/p (arb units)");
  end;

  clear x_lp;
  
  %%fiber
  %%x_lineout = x_laser*sqrt(P_rx/var(x_laser));
  if(~optical_ampl_and_filter_enabled)
    G_opt = 1;
  end;
  
  x_lineout = (x_laser/mean(x_laser))*P_rx*G_opt;%%x_lineout is |xi|^2

  %%take square root to get amplitude signal (unit sqrt(power))
  x_lineout = sqrt(x_lineout);
  
  if(plot_signals)
    figure;
%%%scatterplot(x_lineout(:,1));
    plot(x_lineout(:,1));
    title("line o/p");
  end;
  
  clear x_laser;

  %%optical amplifier
  if(optical_ampl_and_filter_enabled)
    h = 6.62607015e-34;%%plancks constant (already normalized)
    lambda_c = 1550e-9;%%m
    c = 299792458/fs;
    f_c = (c/lambda_c);
    BW_opt_filter = 50e9/fs;%%Hz
    n_sp = 2;

    sigma_n = sqrt(n_sp*h*f_c*(G_opt - 1)*bit_rate)*fs;%%per
    %%polarization (unit sqrt(watt))
    
    %%optical filter
    %%matched filter
    if(supergaussian_optical_filter)
      BW_opt_filter = 50e9/fs;%%passband
      gauss_order = 4;
      
      %% sigma = 1/((2*pi*BW_opt_filter)/(bit_rate*sps));
      sigma = 2*pi*(BW_opt_filter/2)/(2*log(2))^(1/gauss_order);

      n_w = 8*sps;
      w_gauss = single(zeros(n_w,1));
      ind = mod((0:(n_w-1))+ n_w/2, n_w)-n_w/2;

      h_gauss = exp(-1j*2*pi/n_w * (0:(n_w-1))).*...
      exp(-0.5 * ((2*pi*ind/n_w/sigma).^2).^gauss_order); 
      
      w_gauss = real(ifft(h_gauss));
      
      %% normalize to get unit dc gain
      w_gauss = w_gauss/sum(w_gauss);
      x_opt_filter = filter(w_gauss, [1], x_lineout);
      %%x_opt_filter = x_opt_filter(length(w_gauss):end, :);
      clear w_gauss;
      clear h_gauss;
    else
      x_opt_filter = filter(b_r, a_r, x_lineout);
      x_opt_filter = x_opt_filter(length(b_r):end, :);
    end;

    if(plot_signals)
    figure;
%%%scatterplot(x_opt_filter(1+7:sps:length(x_opt_filter),end));
      plot(abs(x_opt_filter(1+7:sps:length(x_opt_filter),end)));
      title("optical filter o/p");
    end;

  else
    x_opt_filter = x_lineout;
    sigma_n = 0;
    G_opt = 1;
  end;
  clear x_lineout;
  
  %%Photodiode
  R = 0.75;%%A/W 
  x_x = x_opt_filter + sigma_n*(randn(length(x_opt_filter), 1) +...
				1j*randn(length(x_opt_filter), 1))/...
		       sqrt(2);
  x_y = sigma_n*(randn(length(x_opt_filter), 1) +...
		 1j*randn(length(x_opt_filter), 1))/sqrt(2);
  x_photodiode = R*G_opt*(x_x.*conj(x_x) + x_y.*conj(x_y));
  
  if(plot_signals)
    figure;
    plot(abs(x_photodiode(1:sps:length(x_photodiode),end)));
    %%scatterplot(x_photodiode(1:sps:length(x_photodiode),end));
    title("photodiode o/p");
  end;

  clear x_opt_filter;
  clear x_x;
  clear x_y;

  x_el_filter_out = single(zeros(size(x_photodiode)));
  if(electrical_ampl_and_filter_enabled)
    %% TIA (electrical amp)
    r_t = 3e3;%%ohm (already normalized)
    if(~disable_electrical_noise)
      s_n = 20e-12;%%A/sqrt(Hz)
    else
      s_n = 0;
    end;
    
    x_tia_out = r_t*(x_photodiode + s_n*sqrt(2*BW_el_filter*fs)*...
				    randn(length(x_photodiode), 1));

    if(plot_signals)
    figure;
      plot(x_tia_out(1:sps:length(x_tia_out),1));
      title("tia o/p");
    end;

    clear x_photodiode;
    
    %% Electrical Filter

    [b_el_s, a_el_s] = besself(4, 1.52*2*pi*BW_el_filter);
    [b_el, a_el] = impinvar(b_el_s, a_el_s);
    x_el_filter_out = filter(b_el, a_el, x_tia_out);

    if(plot_signals)
    figure;
      plot(x_el_filter_out(1:sps:length(x_el_filter_out),1));
      title("electrical filter o/p");
    end;

    clear x_tia_out;
  else
    r_t = 1;
    %%x_el_filter_out = x_tia_out;
    x_el_filter_out = x_photodiode;
    clear x_photodiode;
  end
  
  %%sampling
  %%align
  x_ref = filter(b_r, a_r, x_ps);
  x_ref = x_ref(length(b_r):end);
  [corr, corrLag] = xcorr(x_ref, x_el_filter_out(:,end));
  [corrMax, posCorrMax] = max(abs(corr));
  deltaSamp = corrLag(posCorrMax);
  deltaSamp = -deltaSamp;%%change sign so it's delay
  if(add_sampling_instant_noise)
    deltaSamp = deltaSamp + sigma_sampling_instant_noise*sps;
  end;
  phi_est = angle(corr(posCorrMax));

  %%take integer part
  nDelta = round(deltaSamp);
  %%delay by integer part (append zeros to keep size)
  if(nDelta > 0)
    deltaSamp = deltaSamp - nDelta;
    x_el_filter_out = [x_el_filter_out(((nDelta + 1):end).', :);
		       zeros(nDelta, size(x_el_filter_out, 2))];
  end;

  %%correct the rest by dft interpolation
  if(deltaSamp ~= 0)
    hi = exp(j*(2*pi*(0:(length(x_el_filter_out)-1))*deltaSamp/...
		length(x_el_filter_out)-phi_est)).';
    x_el_filter_out_fft = fft(x_el_filter_out);

    x_el_filter_out_corrected = single(zeros(size(x_el_filter_out)));
  
    bodge_size = size(x_el_filter_out_corrected);

    for i = 1:bodge_size(2)
      %%x_el_filter_out_corrected(:,i) =
      %%real(ifft(hi.*x_el_filter_out_fft(:,i)));
      x_el_filter_out_corrected(:,i) = ...
      abs(ifft(hi.*x_el_filter_out_fft(:,i)));
    end;    

  else
    x_el_filter_out_corrected = x_el_filter_out;
  end;
  
  if(plot_signals)
    figure;
    plot(x_el_filter_out_corrected(1:sps:...
				   length(x_el_filter_out_corrected),...
				   end));
    title("corrected signal");
  end;
  
  x_downsampled =...
  x_el_filter_out_corrected(1:sps:length(x_el_filter_out_corrected)...
			    ,:);
  
  clear x_el_filter_out;
  clear x_ps;
  %%clear x_el_filter_out;
  clear x_ref;
  clear corr;
  clear corrLag;
  clear hi;
  clear x_el_filter_out_fft;

  %%normalization
  for i = 1:length(P_rx)
    %%x_downsampled(:,i) =
    %%x_downsampled(:,i)/sqrt(mean(x_downsampled(:,i).*conj(x_downsampled(:,i))));
    x_el_filter_out_corrected(:,i) = x_el_filter_out_corrected(:,i)/(r_t*P_rx(i)*R*G_opt);
    x_downsampled(:,i) = x_downsampled(:,i)/(r_t*P_rx(i)*R*G_opt);
  end;
  
  if(plot_signals)
    figure;
    scatterplot(x_downsampled(:,end));
    title("downsampled signal (power normalized)");
  end;

end
